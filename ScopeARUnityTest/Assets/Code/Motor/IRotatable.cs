﻿using UnityEngine;

namespace ScopeARTest.Motors
{
    public interface IRotatable
    {
        void Rotate(Vector3 direction, float deltaTime);
    }
}
﻿using UnityEngine;

namespace ScopeARTest.Motors
{
    public interface IMoveable
    {
        void Move(Vector3 direction, float deltaTime);
    }
}
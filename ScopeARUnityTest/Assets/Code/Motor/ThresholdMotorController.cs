﻿using System;
using UnityEngine;

namespace ScopeARTest.Motors
{
    [Serializable]
    public struct ControllerThresholds
    {
        public float Forward;
        public float Backward;
        public float Rotational;

        public ControllerThresholds(float forward, float backward, float rotational)
        {
            Forward = forward;
            Backward = backward;
            Rotational = rotational;
        }
    }

    /// <summary>
    /// Controls a provided motor with thresholds for allowable movement
    /// </summary>
    public class ThresholdMotorController
    {
        private IMotor _motor;

        public ControllerThresholds Thresholds;

        public ThresholdMotorController(IMotor motor) : this(motor, new ControllerThresholds()) { }
        public ThresholdMotorController(IMotor motor, ControllerThresholds thresholds)
        {
            _motor = motor;
            Thresholds = thresholds;
        }

        /// <summary>
        /// Moves motor forward or back based on distance thresholds
        /// </summary>
        public virtual void MoveInDirection(float distance, Vector3 forward, float deltaTime)
        {
            if (distance > Thresholds.Forward)
            {
                _motor.Move(forward, deltaTime);
            }
            else if (distance < Thresholds.Backward)
            {
                _motor.Move(-forward, deltaTime);
            }
        }

        /// <summary>
        /// Rotates motor left or right based on rotational thresholds
        /// </summary>
        public virtual void RotateInDirection(Vector3 direction, Vector3 right, float deltaTime)
        {
            float dotProduct = Vector3.Dot(right, direction);

            if (dotProduct < -Thresholds.Rotational)
            {
                _motor.Rotate(Vector3.down, deltaTime);
            }
            else if (dotProduct > Thresholds.Rotational)
            {
                _motor.Rotate(Vector3.up, deltaTime);
            }
        }
    }
}
﻿using System;
using UnityEngine;

namespace ScopeARTest.Motors
{
    /// <summary>
    /// A motor controlling movement via a provided Character Controller
    /// </summary>
    public class CharacterMotor : BaseBehaviour, IMotor
    {
        [SerializeField]
        private CharacterController _CharacterController;

        [SerializeField]
        private float _MovementSpeed;
        [SerializeField]
        private float _RotationSpeed;
        [SerializeField]
        private float _GravityValue;

        public void Start()
        {
            if (_CharacterController == null)
            {
                throw new NullReferenceException("No CharacterController was provided for CharacterMotor, please assign a CharacterController for the motor to opperate.");
            }
        }
        
        public void Update()
        {
            ApplyGravity();
        }

        private void ApplyGravity()
        {
            Move(Vector3.down * _GravityValue, Time.deltaTime);
        }

        /// <summary>
        /// Moves the character controller in the given direction
        /// </summary>
        public void Move(Vector3 direction, float deltaTime)
        {
            _CharacterController.Move(direction * _MovementSpeed * deltaTime);
        }

        /// <summary>
        /// Rotates the character controller in the given direction
        /// </summary>
        public void Rotate(Vector3 direction, float deltaTime)
        {
            transform.Rotate(direction * _RotationSpeed * deltaTime);
        }
    }
}
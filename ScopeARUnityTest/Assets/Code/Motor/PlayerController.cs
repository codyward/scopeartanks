﻿using UnityEngine;

namespace ScopeARTest.Motors
{
    /// <summary>
    /// Input based motor controller
    /// </summary>
    public class PlayerController : BaseBehaviour
    {
        [SerializeField]
        private KeyCode _KeyForward;
        [SerializeField]
        private KeyCode _KeyBackward;
        [SerializeField]
        private KeyCode _KeyLeft;
        [SerializeField]
        private KeyCode _KeyRight;

        private IMotor _motor;

        public void Start()
        {
            _motor = GetComponent<IMotor>();
        }

        public void Update()
        {
            float deltaTime = Time.deltaTime;

            if (Input.GetKey(_KeyForward))
            {
                _motor.Move(transform.forward, deltaTime);
            }

            if (Input.GetKey(_KeyBackward))
            {
                _motor.Move(-transform.forward, deltaTime);
            }

            if (Input.GetKey(_KeyLeft))
            {
                _motor.Rotate(Vector3.down, deltaTime);
            }

            if (Input.GetKey(_KeyRight))
            {
                _motor.Rotate(Vector3.up, deltaTime);
            }
        }
    }
}
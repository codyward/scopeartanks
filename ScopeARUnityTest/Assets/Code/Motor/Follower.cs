﻿using ScopeARTest.Targeting;
using System.Collections.Generic;
using UnityEngine;

namespace ScopeARTest.Motors
{
    /// <summary>
    /// Follows a target based on category, with specified thresholds for movement
    /// </summary>
    public class Follower : BaseBehaviour
    {
        [SerializeField]
        private TargetCategory _TargetCategory;
        [SerializeField]
        private ControllerThresholds _ControllerThresholds;

        private TargetFinder _targetFinder;
        private ThresholdMotorController _controller;

        public void Start()
        {
            _targetFinder = new TargetFinder();
            _controller = new ThresholdMotorController(GetComponent<IMotor>(), _ControllerThresholds);
        }

        public void Update()
        {
            ITarget target = _targetFinder.FindFirstTarget(_TargetCategory, GameTargets.Manager);

            float deltaTime = Time.deltaTime;

            Vector3 direction = GetDirectionFromTarget(target);
            _controller.RotateInDirection(direction, transform.right, deltaTime);
            _controller.MoveInDirection(direction.magnitude, transform.forward, deltaTime);
        }

        private Vector3 GetDirectionFromTarget(ITarget target)
        {
            if (target == null)
                return Vector3.zero;

            Vector3 direction = (target.Point.position - transform.position);
            return direction;
        }
    }
}
﻿using ScopeARTest.Health;
using UnityEngine;

namespace ScopeARTest.Weapons
{
    /// <summary>
    /// Controlls rigidbody movement, applying damage on impact
    /// </summary>
    public class Projectile : BaseBehaviour, IInitializable
    {
        [SerializeField]
        private Rigidbody _RigidBody;
        [SerializeField]
        private float _DamageAmount;

        public void Initialize()
        {
            _RigidBody.velocity = Vector3.zero;
        }

        /// <summary>
        /// Move this projectile in the given direction
        /// </summary>
        public void Shoot(Vector3 direction, float speed)
        {
            _RigidBody.AddForce(direction * speed, ForceMode.Impulse);
        }

        public void OnCollisionEnter(Collision collision)
        {
            ApplyDamage(collision.gameObject);
            Destroy();
        }

        private void ApplyDamage(GameObject gameObject)
        {
            if (gameObject == null)
                return;

            IHealth health = gameObject.GetComponentInParent<IHealth>();
            if (health == null)
                return;

            health.Modify(_DamageAmount);
        }

        private void Destroy()
        {
            PoolablePrefab poolable = GetComponent<PoolablePrefab>();
            if (poolable == null)
            {
                Destroy(this);
            }
            else
            {
                ObjectPool<PoolablePrefab>.Return(poolable);
            }
        }
    }
}
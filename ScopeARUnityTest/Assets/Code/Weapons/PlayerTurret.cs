﻿using System;
using UnityEngine;

namespace ScopeARTest.Weapons
{
    /// <summary>
    /// An input based turret controller
    /// </summary>
    public class PlayerTurret : BaseBehaviour
    {
        [SerializeField]
        private Turret _Turret;

        [SerializeField]
        private KeyCode _UpKey;
        [SerializeField]
        private KeyCode _DownKey;

        [SerializeField]
        private KeyCode _ShotKey;

        public void Start()
        {
            if (_Turret == null)
            {
                throw new NullReferenceException("No Turret was provided to the PlayerTurret, but one is required.");
            }
        }

        public void Update()
        {
            PollInput();   
        }

        private void PollInput()
        {
            if (Input.GetKey(_UpKey))
            {
                _Turret.MoveUp();
            }

            if (Input.GetKey(_DownKey))
            {
                _Turret.MoveDown();
            }

            if (Input.GetKeyDown(_ShotKey))
            {
                _Turret.Shoot();
            }
        }
    }
}
﻿using System;
using UnityEngine;

namespace ScopeARTest.Weapons
{
    /// <summary>
    /// Uses a turret trajectory to place an indicator object for aiming
    /// </summary>
    public class TrajectoryIndicator : BaseBehaviour
    {
        [SerializeField]
        private GameObject _Indicator;
        [SerializeField]
        private Turret _Turret;
               
        public void Start()
        {
            if (_Indicator == null)
            {
                throw new NullReferenceException("No Indicator was provided but one is required for LineTrajectory.");
            }

            if (_Turret == null)
            {
                throw new NullReferenceException("No Turret was provided but one is required for LineTrajectory.");
            }
        }

        public void Update()
        {
            MoveIndicator();
        }

        private void MoveIndicator()
        {
            RaycastHit hitInfo = _Turret.GetTrajectoryPoint();
            _Indicator.transform.rotation = Quaternion.FromToRotation(Vector3.up, hitInfo.normal);
            _Indicator.transform.position = hitInfo.point;
        }
    }
}
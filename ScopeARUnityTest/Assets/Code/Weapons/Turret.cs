﻿using System;
using UnityEngine;

namespace ScopeARTest.Weapons
{
    /// <summary>
    /// A rotatable weapon which can fire pooled projectiles
    /// </summary>
    public class Turret : BaseBehaviour
    {
        [Header("Aim")]
        [SerializeField]
        private float _MinAngle;
        [SerializeField]
        private float _MaxAngle;

        public float MinAngle { get { return _MinAngle; } }
        public float MaxAngle { get { return _MaxAngle; } }
        public float MidAngle { get { return (_MinAngle + _MaxAngle) / 2; } }

        [SerializeField]
        private float _MovementSpeed;

        [Header("Projectile")]
        [SerializeField]
        private float _ShotSpeed;
        public float ShotSpeed { get { return _ShotSpeed; } }
        [SerializeField]
        private PoolablePrefab _ProjectilePrefab;
        [SerializeField]
        private Transform _ShotPoint;
        public Transform ShotPoint { get { return _ShotPoint; } }
        [SerializeField]
        private LayerMask _TrajectoryMask;

        public Vector3 ShotDirection { get { return transform.up; } }

        private float _rotationAngle;
        public float CurrentAngle { get { return _rotationAngle; } }

        public void Start()
        {
            Move(MidAngle);
        }

        /// <summary>
        /// Rotate upward
        /// </summary>
        public void MoveUp()
        {
            Move(_MovementSpeed * Time.deltaTime);
        }

        /// <summary>
        /// Rotate downward
        /// </summary>
        public void MoveDown()
        {
            Move(-_MovementSpeed * Time.deltaTime);
        }

        private void Move(float amount)
        {
            _rotationAngle += amount;
            _rotationAngle = Mathf.Clamp(_rotationAngle, _MinAngle, _MaxAngle);
            transform.localRotation = Quaternion.Euler(_rotationAngle, 0, 0);
        }
        
        /// <summary>
        /// Fire a pooled projectile
        /// </summary>
        public void Shoot()
        {
            PoolablePrefab prefab = ObjectPool<PoolablePrefab>.GetOrCreate(_ProjectilePrefab);
            Projectile projectile = prefab.GetComponent<Projectile>();
            if (projectile == null)
            {
                throw new NullReferenceException("No projectile was found on the prefab " + prefab.name + " but one was expected. Please attach a projectile script to the prefab.");
            }

            projectile.transform.position = _ShotPoint.position;
            projectile.Shoot(ShotDirection, _ShotSpeed);
        }

        /// <summary>
        /// Returns the estimated location a fired projectile would hit
        /// </summary>
        public RaycastHit GetTrajectoryPoint()
        {
            Vector3 position = ShotPoint.position;
            Vector3 velocity = ShotDirection * ShotSpeed;
            Vector3 gravity = Physics.gravity;

            for (var i = 0; i < 50; i++)
            {
                Vector3 lastPosition = position;
                velocity += gravity * Time.fixedDeltaTime * 5;
                position += velocity * Time.fixedDeltaTime * 5;

                RaycastHit hitInfo;
                if (Physics.Linecast(lastPosition, position, out hitInfo, _TrajectoryMask))
                {
                    return hitInfo;
                }
            }

            return default(RaycastHit);
        }
    }
}
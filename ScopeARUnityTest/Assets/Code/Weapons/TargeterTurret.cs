﻿using ScopeARTest.Targeting;
using System;
using UnityEngine;

namespace ScopeARTest.Weapons
{
    /// <summary>
    /// Target based turret controller
    /// Attempts to aim at target, and fires shots over time
    /// </summary>
    public class TargeterTurret : BaseBehaviour
    {
        [SerializeField]
        private Turret _Turret;

        [SerializeField]
        private TargetCategory _TargetCategory;

        [SerializeField]
        private float _ShotSpeed;

        private TargetFinder _targetFinder;
        private float _lastShotTime;

        private bool CanShoot
        {
            get { return _lastShotTime + _ShotSpeed < Time.time; }
        }

        public void Start()
        {
            if (_Turret == null)
            {
                throw new NullReferenceException("No Turret was provided to the PlayerTurret, but one is required.");
            }

            _targetFinder = new TargetFinder();
        }

        public void Update()
        {
            AimAtTarget(_targetFinder.FindFirstTarget(_TargetCategory, GameTargets.Manager));
            TryShoot();
        }

        private void TryShoot()
        {
            if (CanShoot)
            {
                _Turret.Shoot();
                _lastShotTime = Time.time;
            }
        }
        
        private void AimAtTarget(ITarget target)
        {
            if (target == null)
                return;

            float distanceToTarget = (target.Point.position - transform.position).sqrMagnitude;
            var trajectoryInfo = _Turret.GetTrajectoryPoint();
            float distanceToTrajectory = (trajectoryInfo.point - transform.position).sqrMagnitude;

            if (distanceToTarget > distanceToTrajectory) // extend aim
            {
                if (_Turret.CurrentAngle < _Turret.MidAngle) 
                {
                    _Turret.MoveUp();
                }
                else
                {
                    _Turret.MoveDown();
                }
            }
            else // reduce aim
            {
                if (_Turret.CurrentAngle < _Turret.MaxAngle)
                {
                    _Turret.MoveUp();
                }
                else 
                {
                    _Turret.MoveDown();
                }
            }
        }
    }
}
﻿using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace ScopeARTest.UI
{
    /// <summary>
    /// Provides an OnClick callback for the given button
    /// </summary>
    public abstract class GameButton : BaseBehaviour
    {
        [SerializeField]
        private Button _Button;

        private UnityAction _defaultClickAction;

        public virtual void OnValidate()
        {
            if (_Button == null)
                _Button = GetComponent<Button>();
        }

        public virtual void Awake()
        {
            if (_Button == null)
            {
                throw new NullReferenceException();
            }
        }

        public virtual void Start()
        {
            _defaultClickAction = new UnityAction(OnClick);
            AddClickAction(_defaultClickAction);
        }

        public virtual void OnDestroy()
        {
            RemoveClickAction(_defaultClickAction);
        }

        protected abstract void OnClick();

        public void AddClickAction(UnityAction action)
        {
            if (action == null)
            {
                Debug.LogError("Can not add a null click action");
                return;
            }

            _Button.onClick.AddListener(action);
        }

        public void RemoveClickAction(UnityAction action)
        {
            if (action == null)
            {
                Debug.LogError("Can not remove a null click action");
                return;
            }

            _Button.onClick.RemoveListener(action);
        }
    }
}
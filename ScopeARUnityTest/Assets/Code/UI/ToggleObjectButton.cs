﻿using UnityEngine;

namespace ScopeARTest.UI
{
    /// <summary>
    /// Toggles a gameObject on click
    /// </summary>
    public class ToggleObjectButton : GameButton
    {
        [SerializeField]
        private GameObject _Object;

        protected override void OnClick()
        {
            if (_Object == null)
                return;

            _Object.SetActive(!_Object.activeInHierarchy);
        }
    }
}
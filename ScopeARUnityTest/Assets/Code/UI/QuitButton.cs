﻿using ScopeARTest.Navigation;

namespace ScopeARTest.UI
{
    /// <summary>
    /// Quits the application on click
    /// </summary>
    public class QuitButton : GameButton
    {
        protected override void OnClick()
        {
            NavigationManager.CloseApplication();
        }
    }
}
﻿using ScopeARTest.Navigation;
using UnityEngine;

namespace ScopeARTest.UI
{
    /// <summary>
    /// Uses the Navigation Manager to navigate on click
    /// </summary>
    public class NavigationButton : GameButton
    {
        [SerializeField]
        private NavigationMode _NavigationMode;

        protected override void OnClick()
        {
            NavigationManager.NavigateTo(_NavigationMode);
        }
    }
}
﻿using ScopeARTest.Scoring;
using UnityEngine;
using UnityEngine.UI;

namespace ScopeARTest.UI
{
    /// <summary>
    /// Formats the given Text using the given score mode
    /// </summary>
    public class ScoreText : BaseBehaviour
    {
        [SerializeField]
        private Text _Text;
        [SerializeField]
        private string _FormatString = "Score: {0}";
        [SerializeField]
        private Modes _Mode;
        [SerializeField]
        private int DisabledThreshold = 0;

        public enum Modes
        {
            Score = 0,
            Kills = 1,
            TotalKills = 2,
        }

        private void Update()
        {
            int score = GetScore();
            if (score >= DisabledThreshold)
            {
                _Text.text = string.Format(_FormatString, score);
            }
            else
            {
                _Text.text = string.Empty;
            }
        }

        private int GetScore()
        {
            switch (_Mode)
            {
                case Modes.Score:
                    return ScoreManager.Score;
                case Modes.Kills:
                    return ScoreManager.KillsThisGame;
                case Modes.TotalKills:
                    return ScoreManager.TotalKills;
                default:
                    return 0;
            }
        }
    }
}
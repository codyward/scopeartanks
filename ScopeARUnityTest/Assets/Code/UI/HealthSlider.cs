﻿using ScopeARTest.Health;
using UnityEngine;
using UnityEngine.UI;

namespace ScopeARTest.UI
{
    /// <summary>
    /// A slider to represent health percentage
    /// </summary>
    public class HealthSlider : BaseBehaviour
    {
        [SerializeField]
        private GameObject _Target;

        [SerializeField]
        private Slider _Slider;

        [SerializeField]
        private float _LerpSpeed = 10;

        private IHealth _health;
        private IHealth Health
        {
            get
            {
                if (_health == null && _Target != null)
                {
                    _health = _Target.GetComponent<IHealth>();
                }
                return _health;
            }
        }

        public void Update()
        {
            var health = Health;
            if (health == null)
                return;

            _Slider.value = Mathf.Lerp(_Slider.value, health.CurrentPercent, Time.deltaTime * _LerpSpeed);
        }
    }
}
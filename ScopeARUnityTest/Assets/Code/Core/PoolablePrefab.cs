﻿using System.Collections.Generic;
using UnityEngine;

namespace ScopeARTest
{
    /// <summary>
    /// an IPoolable implementation for Prefabs
    /// </summary>
    public class PoolablePrefab : BaseBehaviour, IPoolable
    {
        [SerializeField]
        private string _ID;
        public string ID { get { return _ID; } }

        private List<IInitializable> _initializables;
        private List<IDisposable> _disposables;

        /// <summary>
        /// Does the given ID match our ID
        /// </summary>
        public bool IsMatch(string id)
        {
            return id == _ID;
        }

        /// <summary>
        /// Enables our object and initializes all children
        /// </summary>
        public void Initialize()
        {
            gameObject.SetActive(true);

            if (_initializables == null)
            {
                _initializables = new List<IInitializable>();
                GetComponentsInChildren(true, _initializables);
            }

            for (int i = 0; i < _initializables.Count; i++)
            {
                var init = _initializables[i];
                init.Initialize();
            }
        }

        /// <summary>
        /// Disables our object and diposes of all children
        /// </summary>
        public void Dispose()
        {
            if (_disposables == null)
            {
                _disposables = new List<IDisposable>();
                GetComponentsInChildren(true, _disposables);
            }

            for (int i = 0; i < _disposables.Count; i++)
            {
                var dispose = _disposables[i];
                dispose.Dispose();
            }

            gameObject.SetActive(false);
        }

        /// <summary>
        /// Create a new instance of this prefab
        /// </summary>
        public IPoolable Create()
        {
            GameObject gameObject = Instantiate(this.gameObject);
            IPoolable poolable = gameObject.GetComponent<IPoolable>();
            return poolable;
        }
    }
}

﻿using System;

namespace ScopeARTest.Exceptions
{
    public class MultipleInstanceException : Exception
    {
        private const string DEFAULT_MESSAGE = "Multiple Instances are not supported.";

        public MultipleInstanceException()
            : this(DEFAULT_MESSAGE) { }

        public MultipleInstanceException(string message) 
            : this(message, null) { }

        public MultipleInstanceException(string message, Exception inner)
            : base(message, inner) { }
    }
}
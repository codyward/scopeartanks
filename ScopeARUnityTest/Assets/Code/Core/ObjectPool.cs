﻿using System;
using System.Collections.Generic;

namespace ScopeARTest
{
    /// <summary>
    /// A static pool of IPoolable
    /// </summary>
    public static class ObjectPool<T> where T : IPoolable
    {
        private static Dictionary<string, List<T>> _objectPool;

        /// <summary>
        /// Tries to retrieve an avialable poolable if one exisits
        /// </summary>
        public static bool GetIfAvailable(string id, out T poolable)
        {
            if (_objectPool == null)
            {
                poolable = default(T);
                return false;
            }

            List<T> pool;

            if (_objectPool.TryGetValue(id, out pool))
            {
                if (pool.Count > 0)
                {
                    poolable = pool[0];
                    pool.RemoveAt(0);
                    poolable.Initialize();
                    return true;
                }
            }

            poolable = default(T);
            return false;
        }

        /// <summary>
        /// Tries to retrieve an available poolable if one exists, creates one otherwise
        /// </summary>
        public static T GetOrCreate(T original)
        {
            if (original == null)
            {
                throw new NullReferenceException("Cannot create a null prefab.");
            }

            if (_objectPool == null)
            {
                _objectPool = new Dictionary<string, List<T>>();
            }

            List<T> pool;
            T poolable;

            if (_objectPool.TryGetValue(original.ID, out pool))
            {
                if (pool.Count > 0)
                {
                    poolable = pool[0];
                    pool.RemoveAt(0);
                    poolable.Initialize();
                    return poolable;
                }
            }

            poolable = (T)original.Create();
            poolable.Initialize();
            return poolable;
        }

        /// <summary>
        /// Return a poolable back to the pool 
        /// </summary>
        public static void Return(T poolable)
        {
            if (poolable == null)
            {
                throw new NullReferenceException("Cannot return a null prefab.");
            }

            poolable.Dispose();

            if (_objectPool == null)
            {
                return;
            }

            List<T> pool;
            if (_objectPool.TryGetValue(poolable.ID, out pool))
            {
                pool.Add(poolable);
            }
            else
            {
                _objectPool[poolable.ID] = new List<T>() { poolable };
            }
        }
    }
}
﻿using UnityEngine;

namespace ScopeARTest
{
    /// <summary>
    /// Forces a GameObject to not Destroy on load, can optionally destroy duplicates
    /// </summary>
    public class DontDestroyOnLoad : BaseBehaviour
    {
        [SerializeField]
        private bool _DestroyDuplicate;

        private bool _isPrefab = true;

        public void Awake()
        {
            // a bit of a hack to determine if a found object is a prefab or not
            _isPrefab = false;

            if (CheckDuplicates())
                return;

            DontDestroyOnLoad(this);
        }

        private bool CheckDuplicates()
        {
            if (_DestroyDuplicate)
            {
                var all = Resources.FindObjectsOfTypeAll(typeof(DontDestroyOnLoad)) as DontDestroyOnLoad[];
                int count = 0;
                for (int i = 0; i < all.Length; i++)
                {
                    var item = all[i];
                    if (item._isPrefab)
                        continue;

                    count++;
                }

                if (count > 1)
                {
                    DestroyImmediate(gameObject);
                    return true;
                }
            }

            return false;
        }
    }
}
﻿namespace ScopeARTest
{
    public interface IPoolable
    {
        string ID { get; }
        IPoolable Create();

        void Initialize();
        void Dispose();
    }
}
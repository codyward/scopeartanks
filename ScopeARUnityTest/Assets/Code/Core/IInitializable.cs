﻿namespace ScopeARTest
{
    public interface IInitializable
    {
        void Initialize();
    }
}
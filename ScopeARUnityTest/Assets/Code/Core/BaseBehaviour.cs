﻿using UnityEngine;

namespace ScopeARTest
{
    /// <summary>
    /// Wrapper for the Unity MonoBehaviour
    /// </summary>
    public abstract class BaseBehaviour : MonoBehaviour
    {
        
    }
}
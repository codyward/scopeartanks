﻿namespace ScopeARTest
{
    public interface IDisposable
    {
        void Dispose();
    }
}
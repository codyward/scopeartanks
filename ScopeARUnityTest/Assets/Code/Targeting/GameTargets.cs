﻿using ScopeARTest.Exceptions;

namespace ScopeARTest.Targeting
{
    /// <summary>
    /// Holds the TargetManager instance for the game
    /// </summary>
    public class GameTargets : BaseBehaviour
    {
        private static GameTargets _instance;

        private TargetManager _targetManager;
        public static TargetManager Manager
        {
            get { return _instance._targetManager; }
        }

        public void Awake()
        {
            if (_instance != null)
            {
                throw new MultipleInstanceException();
            }

            _instance = this;
            _targetManager = new TargetManager();
        }

        public void OnDestroy()
        {
            if (_instance == this)
            {
                _instance = null;
            }
        }
    }
}
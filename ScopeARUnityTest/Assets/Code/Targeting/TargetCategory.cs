﻿using System.Collections.Generic;

namespace ScopeARTest.Targeting
{
    public enum TargetCategory : int
    {
        Unassigned = 0,

        Player  = 1,
        Enemy   = 2,
    }

    public class TargetCategoryComparer : IComparer<TargetCategory>, IEqualityComparer<TargetCategory>
    {
        public int Compare(TargetCategory x, TargetCategory y)
        {
            if (x > y)
                return 1;
            if (x < y)
                return -1;
            if (x == y)
                return 0;

            return -1;
        }

        public bool Equals(TargetCategory x, TargetCategory y)
        {
            return x == y;
        }

        public int GetHashCode(TargetCategory obj)
        {
            return ((int)obj).GetHashCode();
        }
    }
}
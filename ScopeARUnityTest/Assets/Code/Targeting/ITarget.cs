﻿using UnityEngine;

namespace ScopeARTest.Targeting
{
    public interface ITarget
    {
        TargetCategory Category { get; }
        Transform Point { get; }
    }
}
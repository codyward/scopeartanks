﻿using UnityEngine;

namespace ScopeARTest.Targeting
{
    /// <summary>
    /// A Base implementation of ITarget, registers/unregisters when created and destroyed
    /// </summary>
    public class BaseTarget : BaseBehaviour, ITarget
    {
        [SerializeField]
        private TargetCategory _Category;
        public TargetCategory Category
        {
            get { return _Category; }
        }

        public Transform Point
        {
            get { return transform; }
        }

        public void Start()
        {
            GameTargets.Manager.RegisterTarget(this);
        }

        public void OnDestroy()
        {
            GameTargets.Manager.UnregisterTarget(this);
        }
    }
}

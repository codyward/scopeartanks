﻿using System;
using System.Collections.Generic;

namespace ScopeARTest.Targeting
{
    /// <summary>
    /// Holds registered targets based on category, for later retrieval
    /// </summary>
    public class TargetManager
    {
        private Dictionary<TargetCategory, List<ITarget>> _targets;

        public TargetManager()
        {
            _targets = new Dictionary<TargetCategory, List<ITarget>>(new TargetCategoryComparer());
        }
        
        /// <summary>
        /// Register a target to this manager based on category
        /// </summary>
        public void RegisterTarget(ITarget target)
        {
            if (target == null)
            {
                throw new NullReferenceException("Cannot register a null target.");
            }

            List<ITarget> targets;
            if (!_targets.TryGetValue(target.Category, out targets))
            {
                targets = new List<ITarget>() { target };
                _targets[target.Category] = targets;
            }

            if (targets.Contains(target))
            {
                // avoid duplicates
                return;
            }

            targets.Add(target);
        }

        /// <summary>
        /// Unregister a target from this manager
        /// </summary>
        public void UnregisterTarget(ITarget target)
        {
            if (target == null)
            {
                throw new NullReferenceException("Cannot unregister a null target.");
            }

            List<ITarget> targets;
            if (!_targets.TryGetValue(target.Category, out targets))
            {
                return;
            }

            targets.Remove(target);
        }

        /// <summary>
        /// Retrieves all targets of a given category, if any exist
        /// </summary>
        public List<ITarget> GetTargets(TargetCategory category)
        {
            List<ITarget> targets;
            _targets.TryGetValue(category, out targets);
            return targets;
        }
    }
}
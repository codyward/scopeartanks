﻿using System.Collections.Generic;

namespace ScopeARTest.Targeting
{
    /// <summary>
    /// Utility for finding targets from a provided manager
    /// </summary>
    public class TargetFinder
    {
        private List<ITarget> _targets;

        public TargetFinder()
        {
            _targets = new List<ITarget>();
        }
        
        public List<ITarget> FindTargets(TargetCategory category, TargetManager manager)
        {
            if (manager != null)
            {
                _targets = manager.GetTargets(category);
            }
            else
            {
                _targets = null;
            }

            return _targets;
        }

        public ITarget FindFirstTarget(TargetCategory category, TargetManager manager)
        {
            FindTargets(category, manager);
            if (_targets == null || _targets.Count <= 0)
                return null;

            return _targets[0];
        }
    }
}
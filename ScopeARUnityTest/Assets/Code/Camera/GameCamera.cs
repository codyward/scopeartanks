﻿using System;
using UnityEngine;

namespace ScopeARTest.Camera
{
    /// <summary>
    /// A Camera which follows a given point, and looks at a given target
    /// </summary>
    public class GameCamera : BaseBehaviour
    {
        [SerializeField]
        private Transform _FollowTarget;
        [SerializeField]
        private Transform _LookTarget;

        [SerializeField]
        private float _FollowSpeed;
        [SerializeField]
        private float _LookSpeed;

        public void Start()
        {
            if (_FollowTarget == null)
            {
                throw new NullReferenceException("Follow Target must not be null in order for GameCamera to opperate. Please assign a valid target to follow.");
            }

            if (_LookTarget == null)
            {
                throw new NullReferenceException("Look Target must not be null in order for GameCamera to opperate. Please assign a valid target to look at.");
            }
        }

        public void Update()
        {
            float deltaTime = Time.deltaTime;

            UpdatePosition(deltaTime);
            UpdateRotation(deltaTime);
        }

        private void UpdatePosition(float deltaTime)
        {
            Vector3 position = transform.position;
            position = Vector3.Lerp(position, _FollowTarget.position, deltaTime * _FollowSpeed);
            transform.position = position;
        }

        private void UpdateRotation(float deltaTime)
        {
            Quaternion targetRotation = Quaternion.LookRotation(_LookTarget.position - transform.position);
            transform.rotation = Quaternion.Lerp(transform.rotation, targetRotation, deltaTime * _LookSpeed);
        }
    }
}
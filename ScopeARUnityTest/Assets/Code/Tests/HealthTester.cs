﻿using NUnit.Framework;
using ScopeARTest.Health;
using UnityEngine;

namespace ScopeARTest.Testing
{
    [Category("Health")]
    public class HealthTester
    {
        [Test]
        public void HealthCanDecrease()
        {
            GameObject gameObject = new GameObject("Health Object", typeof(SimpleHealth));
            IHealth health = gameObject.GetComponent<IHealth>();
            health.SetMaximum(100);

            health.Modify(-10);

            Assert.AreEqual(health.CurrentHealth, 90);
        }

        [Test]
        public void HealthCanIncrease()
        {
            GameObject gameObject = new GameObject("Health Object", typeof(SimpleHealth));
            IHealth health = gameObject.GetComponent<IHealth>();
            health.SetMaximum(100);

            health.Modify(-10);
            health.Modify(10);

            Assert.AreEqual(health.CurrentHealth, 100);
        }

        [Test]
        public void HealthHasMaximum()
        {
            GameObject gameObject = new GameObject("Health Object", typeof(SimpleHealth));
            IHealth health = gameObject.GetComponent<IHealth>();
            health.SetMaximum(100);

            health.Modify(10);

            Assert.AreEqual(health.CurrentHealth, 100);
        }

        [Test]
        public void HealthHasMinimum()
        {
            GameObject gameObject = new GameObject("Health Object", typeof(SimpleHealth));
            IHealth health = gameObject.GetComponent<IHealth>();
            health.SetMaximum(100);

            health.Modify(-1000);

            Assert.AreEqual(health.CurrentHealth, 0);
        }

        [Test]
        public void HealthIsUnaffectedByNoModification()
        {
            GameObject gameObject = new GameObject("Health Object", typeof(SimpleHealth));
            IHealth health = gameObject.GetComponent<IHealth>();
            health.SetMaximum(100);

            health.Modify(0);

            Assert.AreEqual(health.CurrentHealth, 100);
        }

        [Test]
        public void HealthCalculatesPercentage()
        {
            GameObject gameObject = new GameObject("Health Object", typeof(SimpleHealth));
            IHealth health = gameObject.GetComponent<IHealth>();
            health.SetMaximum(100);

            health.Modify(-50);

            Assert.AreEqual(health.CurrentPercent, 0.5f);
        }

        [Test]
        public void HealthCallsDeathAtZeroExact()
        {
            GameObject gameObject = new GameObject("Health Object", typeof(SimpleHealth));
            IHealth health = gameObject.GetComponent<IHealth>();
            health.SetMaximum(100);
            bool deathWasCalled = false;
            health.OnDeath += () => deathWasCalled = true;

            health.Modify(-100);

            Assert.IsTrue(deathWasCalled);
        }

        [Test]
        public void HealthCallsDeathAtZeroOverflow()
        {
            GameObject gameObject = new GameObject("Health Object", typeof(SimpleHealth));
            IHealth health = gameObject.GetComponent<IHealth>();
            health.SetMaximum(100);
            bool deathWasCalled = false;
            health.OnDeath += () => deathWasCalled = true;

            health.Modify(-1000);

            Assert.IsTrue(deathWasCalled);
        }

        [Test]
        public void HealthDoesNotCallDeathIfNotZero()
        {
            GameObject gameObject = new GameObject("Health Object", typeof(SimpleHealth));
            IHealth health = gameObject.GetComponent<IHealth>();
            health.SetMaximum(100);
            bool deathWasCalled = false;
            health.OnDeath += () => deathWasCalled = true;

            health.Modify(-10);

            Assert.IsFalse(deathWasCalled);
        }

        [Test]
        public void HealthDoesNotCallDeathTwiceIfNotRevived()
        {
            GameObject gameObject = new GameObject("Health Object", typeof(SimpleHealth));
            IHealth health = gameObject.GetComponent<IHealth>();
            health.SetMaximum(100);
            int deathCount = 0;
            health.OnDeath += () => deathCount++;

            health.Modify(-100); //once
            health.Modify(-100); //twice

            Assert.AreEqual(deathCount, 1);
        }

        [Test]
        public void HealthCallsDeathTwiceIfRevived()
        {
            GameObject gameObject = new GameObject("Health Object", typeof(SimpleHealth));
            IHealth health = gameObject.GetComponent<IHealth>();
            health.SetMaximum(100);
            int deathCount = 0;
            health.OnDeath += () => deathCount++;

            health.Modify(-100); // kill
            health.Modify(100);  // revive
            health.Modify(-100); // kill again

            Assert.AreEqual(deathCount, 2);
        }
    }
}
﻿using NUnit.Framework;
using ScopeARTest.Targeting;
using UnityEngine;

namespace ScopeARTest.Testing
{
    public class TestTarget : ITarget
    {
        private TargetCategory _category;
        public TargetCategory Category { get { return _category; } }

        public Transform Point { get { return null; } }

        public TestTarget(TargetCategory category)
        {
            _category = category;
        }
    }

    [Category("Targets")]
    public class TargetTester
    {
        [Test]
        public void LowerCategoryComparesLower()
        {
            TargetCategoryComparer comparer = new TargetCategoryComparer();

            var comparison = comparer.Compare(TargetCategory.Player, TargetCategory.Enemy);

            Assert.IsTrue(comparison == -1);
        }

        [Test]
        public void HigherCategoryComparesHigher()
        {
            TargetCategoryComparer comparer = new TargetCategoryComparer();

            var comparison = comparer.Compare(TargetCategory.Enemy, TargetCategory.Player);

            Assert.IsTrue(comparison == 1);
        }

        [Test]
        public void SameCategoryComparesEqual()
        {
            TargetCategoryComparer comparer = new TargetCategoryComparer();

            var comparison = comparer.Compare(TargetCategory.Enemy, TargetCategory.Enemy);

            Assert.IsTrue(comparison == 0);
        }

        [Test]
        public void CanAddSingleTargetToManager()
        {
            TargetManager manager = new TargetManager();
            ITarget target = new TestTarget(TargetCategory.Unassigned);

            manager.RegisterTarget(target);
            var retrieved = manager.GetTargets(TargetCategory.Unassigned);

            bool found = false;
            if (retrieved != null && retrieved.Count == 1)
            {
                found = retrieved[0] == target;
            }

            Assert.IsTrue(found);
        }

        [Test]
        public void CanAddMultipleTargetsToManager()
        {
            TargetManager manager = new TargetManager();
            ITarget targetA = new TestTarget(TargetCategory.Unassigned);
            ITarget targetB = new TestTarget(TargetCategory.Unassigned);

            manager.RegisterTarget(targetA);
            manager.RegisterTarget(targetB);
            var retrieved = manager.GetTargets(TargetCategory.Unassigned);

            bool found = false;
            if (retrieved != null && retrieved.Count == 2)
            {
                found = retrieved[0] == targetA;
                found &= retrieved[1] == targetB;
            }

            Assert.IsTrue(found);
        }

        [Test]
        public void CanRemoveFirstTargetFromManager()
        {
            TargetManager manager = new TargetManager();
            ITarget targetA = new TestTarget(TargetCategory.Unassigned);
            ITarget targetB = new TestTarget(TargetCategory.Unassigned);

            manager.RegisterTarget(targetA);
            manager.RegisterTarget(targetB);

            manager.UnregisterTarget(targetA);
            var retrieved = manager.GetTargets(TargetCategory.Unassigned);

            bool found = false;
            if (retrieved != null && retrieved.Count == 1)
            {
                found = retrieved[0] == targetB;
            }

            Assert.IsTrue(found);
        }

        [Test]
        public void CanRemoveSecondTargetFromManager()
        {
            TargetManager manager = new TargetManager();
            ITarget targetA = new TestTarget(TargetCategory.Unassigned);
            ITarget targetB = new TestTarget(TargetCategory.Unassigned);

            manager.RegisterTarget(targetA);
            manager.RegisterTarget(targetB);

            manager.UnregisterTarget(targetB);
            var retrieved = manager.GetTargets(TargetCategory.Unassigned);

            bool found = false;
            if (retrieved != null && retrieved.Count == 1)
            {
                found = retrieved[0] == targetA;
            }

            Assert.IsTrue(found);
        }

        [Test]
        public void CanRemoveAllTargetsFromManager()
        {
            TargetManager manager = new TargetManager();
            ITarget targetA = new TestTarget(TargetCategory.Unassigned);
            ITarget targetB = new TestTarget(TargetCategory.Unassigned);

            manager.RegisterTarget(targetA);
            manager.RegisterTarget(targetB);

            manager.UnregisterTarget(targetA);
            manager.UnregisterTarget(targetB);

            var retrieved = manager.GetTargets(TargetCategory.Unassigned);

            bool found = false;
            if (retrieved == null || retrieved.Count == 0)
            {
                found = true;
            }

            Assert.IsTrue(found);
        }

        [Test]
        public void CannotAddDuplicatesToManager()
        {
            TargetManager manager = new TargetManager();
            ITarget targetA = new TestTarget(TargetCategory.Unassigned);

            manager.RegisterTarget(targetA);
            manager.RegisterTarget(targetA);

            var retrieved = manager.GetTargets(TargetCategory.Unassigned);

            bool found = false;
            if (retrieved != null && retrieved.Count == 1)
            {
                found = true;
            }

            Assert.IsTrue(found);
        }

        [Test]
        public void CannotRemoveTwiceFromManager()
        {
            TargetManager manager = new TargetManager();
            ITarget targetA = new TestTarget(TargetCategory.Unassigned);
            ITarget targetB = new TestTarget(TargetCategory.Unassigned);

            manager.RegisterTarget(targetA);
            manager.RegisterTarget(targetB);

            manager.UnregisterTarget(targetA);
            manager.UnregisterTarget(targetA);

            var retrieved = manager.GetTargets(TargetCategory.Unassigned);

            bool found = false;
            if (retrieved != null && retrieved.Count == 1)
            {
                found = retrieved[0] == targetB;
            }

            Assert.IsTrue(found);
        }

        [Test]
        public void TargetFinderFindsFirstTarget()
        {
            TargetManager manager = new TargetManager();
            ITarget targetA = new TestTarget(TargetCategory.Unassigned);
            ITarget targetB = new TestTarget(TargetCategory.Unassigned);

            manager.RegisterTarget(targetA);
            manager.RegisterTarget(targetB);

            TargetFinder finder = new TargetFinder();

            var retrieved = finder.FindFirstTarget(TargetCategory.Unassigned, manager);

            bool found = retrieved == targetA;

            Assert.IsTrue(found);
        }
    }
}
﻿using NUnit.Framework;
using ScopeARTest.Motors;
using UnityEngine;

namespace ScopeARTest.Testing
{
    public class TestMotor : IMotor
    {
        public Vector3 Position;
        public Quaternion Rotation;

        public TestMotor()
        {
            Position = Vector3.zero;
            Rotation = Quaternion.identity;
        }

        public void Move(Vector3 direction, float deltaTime)
        {
            Position += direction;
        }

        public void Rotate(Vector3 direction, float deltaTime)
        {
            Rotation = Quaternion.Euler(Rotation.eulerAngles + direction);
        }
    }

    [Category("Motors")]
    public class MotorTester
    {
        #region No Thresholds
        [Test]
        public void NoThresholdCanMoveForward()
        {
            TestMotor motor = new TestMotor();
            ControllerThresholds thresholds = new ControllerThresholds(0, 0, 0);
            ThresholdMotorController controller = new ThresholdMotorController(motor, thresholds);

            controller.MoveInDirection(1, Vector3.forward, 0);

            Assert.AreEqual(motor.Position, Vector3.forward);
        }

        [Test]
        public void NoThresholdCanMoveBackward()
        {
            TestMotor motor = new TestMotor();
            ControllerThresholds thresholds = new ControllerThresholds(0, 0, 0);
            ThresholdMotorController controller = new ThresholdMotorController(motor, thresholds);

            controller.MoveInDirection(1, Vector3.back, 0);

            Assert.AreEqual(motor.Position, Vector3.back);
        }

        [Test]
        public void NoThresholdCanRotateRight()
        {
            TestMotor motor = new TestMotor();
            ControllerThresholds thresholds = new ControllerThresholds(0, 0, 0);
            ThresholdMotorController controller = new ThresholdMotorController(motor, thresholds);

            controller.RotateInDirection(Vector3.right, Vector3.right, 0);
            Quaternion expected = Quaternion.Euler(new Vector3(0, 1, 0));

            Assert.AreEqual(expected, motor.Rotation);
        }

        [Test]
        public void NoThresholdCanRotateLeft()
        {
            TestMotor motor = new TestMotor();
            ControllerThresholds thresholds = new ControllerThresholds(0, 0, 0);
            ThresholdMotorController controller = new ThresholdMotorController(motor, thresholds);

            controller.RotateInDirection(Vector3.left, Vector3.right, 0);
            Quaternion expected = Quaternion.Euler(new Vector3(0, -1, 0));

            Assert.AreEqual(expected, motor.Rotation);
        }

        #endregion

        #region Thresholds

        #region Beyond Threshold
        [Test]
        public void WithThresholdCanMoveForward()
        {
            TestMotor motor = new TestMotor();
            ControllerThresholds thresholds = new ControllerThresholds(20, 10, 0.1f);
            ThresholdMotorController controller = new ThresholdMotorController(motor, thresholds);

            controller.MoveInDirection(21, Vector3.forward, 0);

            Assert.AreEqual(motor.Position, Vector3.forward);
        }

        [Test]
        public void WithThresholdCanMoveBackward()
        {
            TestMotor motor = new TestMotor();
            ControllerThresholds thresholds = new ControllerThresholds(20, 10, 0.1f);
            ThresholdMotorController controller = new ThresholdMotorController(motor, thresholds);

            controller.MoveInDirection(5, Vector3.forward, 0);

            Assert.AreEqual(motor.Position, Vector3.back);
        }

        [Test]
        public void WithThresholdCanRotateRight()
        {
            TestMotor motor = new TestMotor();
            ControllerThresholds thresholds = new ControllerThresholds(20, 10, 0.1f);
            ThresholdMotorController controller = new ThresholdMotorController(motor, thresholds);

            controller.RotateInDirection(Vector3.right, Vector3.right, 0);
            Quaternion expected = Quaternion.Euler(new Vector3(0, 1, 0));

            Assert.AreEqual(expected, motor.Rotation);
        }

        [Test]
        public void WithThresholdCanRotateLeft()
        {
            TestMotor motor = new TestMotor();
            ControllerThresholds thresholds = new ControllerThresholds(20, 10, 0.1f);
            ThresholdMotorController controller = new ThresholdMotorController(motor, thresholds);

            controller.RotateInDirection(Vector3.left, Vector3.right, 0);
            Quaternion expected = Quaternion.Euler(new Vector3(0, -1, 0));

            Assert.AreEqual(expected, motor.Rotation);
        }

        #endregion

        #region Within Threshold
        [Test]
        public void WithThresholdCannotMoveForward()
        {
            TestMotor motor = new TestMotor();
            ControllerThresholds thresholds = new ControllerThresholds(20, 10, 2f);
            ThresholdMotorController controller = new ThresholdMotorController(motor, thresholds);

            controller.MoveInDirection(15, Vector3.forward, 0);

            Assert.AreEqual(motor.Position, Vector3.zero);
        }

        [Test]
        public void WithThresholdCannotMoveBackward()
        {
            TestMotor motor = new TestMotor();
            ControllerThresholds thresholds = new ControllerThresholds(20, 10, 2f);
            ThresholdMotorController controller = new ThresholdMotorController(motor, thresholds);

            controller.MoveInDirection(15, Vector3.forward, 0);

            Assert.AreEqual(motor.Position, Vector3.zero);
        }

        [Test]
        public void WithThresholdCannotRotateRight()
        {
            TestMotor motor = new TestMotor();
            ControllerThresholds thresholds = new ControllerThresholds(20, 10, 2f);
            ThresholdMotorController controller = new ThresholdMotorController(motor, thresholds);

            controller.RotateInDirection(Vector3.right, Vector3.right, 0);
            Quaternion expected = Quaternion.identity;

            Assert.AreEqual(expected, motor.Rotation);
        }

        [Test]
        public void WithThresholdCannotRotateLeft()
        {
            TestMotor motor = new TestMotor();
            ControllerThresholds thresholds = new ControllerThresholds(20, 10, 2f);
            ThresholdMotorController controller = new ThresholdMotorController(motor, thresholds);

            controller.RotateInDirection(Vector3.left, Vector3.right, 0);
            Quaternion expected = Quaternion.identity;

            Assert.AreEqual(expected, motor.Rotation);
        }

        #endregion

        #endregion
    }
}
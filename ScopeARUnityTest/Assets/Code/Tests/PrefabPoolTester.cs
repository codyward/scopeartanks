﻿using NUnit.Framework;

namespace ScopeARTest.Testing
{
    public class PoolableDummy : IPoolable
    {
        public PoolableDummy(string id)
        {
            _id = id;
        }

        private string _id;
        public string ID
        {
            get { return _id; }
        }

        public IPoolable Create() { return new PoolableDummy(ID); }

        public void Dispose() { }

        public void Initialize() { }
    }

    [Category("Pooling")]
    public class PrefabPoolTester
    {
        [Test]
        public void CreatePoolableObject()
        {
            PoolableDummy originalDummy = new PoolableDummy("dummy");
            IPoolable poolable = ObjectPool<IPoolable>.GetOrCreate(originalDummy);

            Assert.NotNull(poolable);
        }

        [Test]
        public void CreateNewPoolableObject()
        {
            PoolableDummy originalDummy = new PoolableDummy("dummy");
            IPoolable poolable = ObjectPool<IPoolable>.GetOrCreate(originalDummy);

            Assert.AreNotEqual(poolable, originalDummy);
        }

        [Test]
        public void ReturnPoolableObject()
        {
            PoolableDummy dummy = new PoolableDummy("dummy");
            ObjectPool<IPoolable>.Return(dummy);

            // check to ensure it made it to the pool and is accessible
            IPoolable poolable;
            bool success = ObjectPool<IPoolable>.GetIfAvailable("dummy", out poolable);

            Assert.True(success && poolable != null);
        }
    }
}

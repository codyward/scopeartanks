﻿namespace ScopeARTest.Health
{
    public class ReviveOnDeath : BaseDeathBehaviour
    {
        protected override void OnDeath()
        {
            Health.Modify(Health.MaxHealth);
        }
    }
}
﻿using System;
using UnityEngine;

namespace ScopeARTest.Health
{
    /// <summary>
    /// A simple IHealth implementation which calls death when health drops to 0
    /// </summary>
    public class SimpleHealth : BaseBehaviour, IHealth
    {
        [SerializeField]
        private float _MaxHealth;
        public float MaxHealth
        {
            get { return _MaxHealth; }
        }

        [SerializeField]
        private float _CurrentHealth;
        public float CurrentHealth
        {
            get { return _CurrentHealth; }
        }

        public float CurrentPercent
        {
            get { return _CurrentHealth / _MaxHealth; }
        }

        public bool IsAlive { get { return _CurrentHealth > 0; } }
        public bool IsDead { get { return !IsAlive; } }

        private Action _onDeath;
        public Action OnDeath { get { return _onDeath; } set { _onDeath = value; } }
        
        /// <summary>
        /// Modify our health by the given modification amount, clamping, and calling death when appropriate
        /// </summary>
        public virtual void Modify(float modification)
        {
            float oldHealth = _CurrentHealth;
            _CurrentHealth += modification;

            _CurrentHealth = Mathf.Clamp(_CurrentHealth, 0, _MaxHealth);

            if (oldHealth > _CurrentHealth && _CurrentHealth <= 0)
            {
                TriggerDeath(); 
            }
        }

        /// <summary>
        /// Sets our maximum health value, and adjusts current to match
        /// </summary>
        public void SetMaximum(float maximumHealth)
        {
            _MaxHealth = maximumHealth;
            _CurrentHealth = MaxHealth;
        }

        protected virtual void TriggerDeath()
        {
            if (_onDeath != null)
            {
                _onDeath();
            }
        }
    }
}
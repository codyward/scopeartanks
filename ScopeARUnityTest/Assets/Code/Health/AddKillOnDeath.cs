﻿using ScopeARTest.Scoring;

namespace ScopeARTest.Health
{
    public class AddKillOnDeath : BaseDeathBehaviour
    {
        protected override void OnDeath()
        {
            ScoreManager.AddKill();
        }
    }
}
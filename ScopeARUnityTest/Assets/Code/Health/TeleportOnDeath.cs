﻿using UnityEngine;

namespace ScopeARTest.Health
{
    public class TeleportOnDeath : BaseDeathBehaviour
    {
        [SerializeField]
        private Vector2 _RangeX;
        [SerializeField]
        private Vector2 _RangeZ;
        [SerializeField]
        private float _Height;

        [SerializeField]
        private Transform _ToTeleport;

        protected override void OnDeath()
        {
            Vector3 teleportPosition = new Vector3(Random.Range(_RangeX.x, _RangeX.y), _Height, Random.Range(_RangeZ.x, _RangeZ.y));
            _ToTeleport.transform.position = teleportPosition;
        }
    }
}
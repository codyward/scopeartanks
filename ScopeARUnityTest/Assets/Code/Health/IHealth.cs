﻿using System;

namespace ScopeARTest.Health
{
    public interface IHealth
    {
        float MaxHealth { get; }
        float CurrentHealth { get; }
        float CurrentPercent { get; }

        bool IsAlive { get; }
        bool IsDead { get; }

        Action OnDeath { get; set; }

        void Modify(float modification);
        void SetMaximum(float maximumHealth);
    }
}
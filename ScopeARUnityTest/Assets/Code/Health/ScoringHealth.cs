﻿using ScopeARTest.Scoring;

namespace ScopeARTest.Health
{
    public class ScoringHealth : SimpleHealth
    {
        public override void Modify(float modification)
        {
            float previousHealth = CurrentHealth;

            base.Modify(modification);

            float actualModified = CurrentHealth - previousHealth;
            if (IsValueDamaging(actualModified))
                ScoreManager.AddScore((int)-actualModified);
        }

        private bool IsValueDamaging(float value)
        {
            return value < 0;
        }
    }
}
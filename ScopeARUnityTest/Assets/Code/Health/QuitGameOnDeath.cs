﻿using ScopeARTest.Navigation;

namespace ScopeARTest.Health
{
    public class QuitGameOnDeath : BaseDeathBehaviour
    {
        protected override void OnDeath()
        {
            NavigationManager.NavigateTo(NavigationMode.Main);
        }
    }
}
﻿using System;

namespace ScopeARTest.Health
{
    /// <summary>
    /// Base implementation of a death behaviour, subscribing to the death callback of the attached health component
    /// </summary>
    public abstract class BaseDeathBehaviour : BaseBehaviour
    {
        private IHealth _health;
        protected IHealth Health
        {
            get
            {
                if (_health == null)
                {
                    _health = GetComponent<IHealth>();
                }
                return _health;
            }
        }

        public virtual void Start()
        {
            var health = Health;
            if (health == null)
            {
                throw new NullReferenceException("No IHealth was found, please ensure a valid health script is attached to this object, in order for DeathBehaviour to work.");
            }

            health.OnDeath += OnDeath;
        }

        public virtual void OnDestroy()
        {
            var health = Health;
            if (health == null)
            {
                return;
            }

            health.OnDeath -= OnDeath;
        }

        protected abstract void OnDeath();
    }
}
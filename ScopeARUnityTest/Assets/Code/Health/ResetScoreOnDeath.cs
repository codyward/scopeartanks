﻿using ScopeARTest.Scoring;

namespace ScopeARTest.Health
{
    public class ResetScoreOnDeath : BaseDeathBehaviour
    {
        protected override void OnDeath()
        {
            ScoreManager.ResetScore();
        }
    }
}
﻿using ScopeARTest.Exceptions;
using UnityEngine;

namespace ScopeARTest.Scoring
{
    /// <summary>
    /// Maintains player scores
    /// </summary>
    public class ScoreManager : BaseBehaviour
    {
        private static ScoreManager _instance;

        private const string KEY_KILLS = "kills";
        /// <summary>
        /// Total Kills, persistent between play sessions
        /// </summary>
        public static int TotalKills
        {
            get { return PlayerPrefs.GetInt(KEY_KILLS); }
            private set { PlayerPrefs.SetInt(KEY_KILLS, value); }
        }

        private int _killsThisGame;
        /// <summary>
        /// Kills for the current game session
        /// </summary>
        public static int KillsThisGame
        {
            get { return _instance._killsThisGame; }
            private set { _instance._killsThisGame = value; }
        }

        private int _score;
        /// <summary>
        /// Score for the current game session
        /// </summary>
        public static int Score
        {
            get { return _instance._score; }
            private set { _instance._score = value; }
        }

        public void Awake()
        {
            if (_instance != null)
            {
                throw new MultipleInstanceException();
            }

            _instance = this;

            ResetScore();
        }

        public void OnDestroy()
        {
            if (_instance == null)
            {
                return;
            }

            _instance = null;
        }

        /// <summary>
        /// Adds amount to player score
        /// </summary>
        public static void AddScore(int amount)
        {
            Score += amount;
        }

        /// <summary>
        /// Adds to both current kills and total kills
        /// </summary>
        public static void AddKill()
        {
            TotalKills++;
            KillsThisGame++;
        }

        /// <summary>
        /// Resets current score and kills
        /// </summary>
        public static void ResetScore()
        {
            Score = 0;
            KillsThisGame = 0;
        }
    }
}
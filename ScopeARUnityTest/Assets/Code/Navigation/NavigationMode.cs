﻿namespace ScopeARTest.Navigation
{
    public enum NavigationMode
    {
        Unassigned  = 0,

        Main    = 1,
        Game    = 2,
    }
}
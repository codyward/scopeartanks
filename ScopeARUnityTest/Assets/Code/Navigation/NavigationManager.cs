﻿using ScopeARTest.Exceptions;
using System;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace ScopeARTest.Navigation
{
    /// <summary>
    /// Controls navigation of application between scenes
    /// </summary>
    public class NavigationManager : BaseBehaviour
    {
        private static NavigationManager _instance;

        [SerializeField]
        private string _MainScene = "Main";
        [SerializeField]
        private string _GameScene = "Game";

        private NavigationMode _currentNavigationMode = NavigationMode.Main;

        public void Awake()
        {
            if (_instance != null)
            {
                throw new MultipleInstanceException();
            }

            _instance = this;
        }

        public void OnDestroy()
        {
            if (_instance == null)
            {
                return;
            }

            _instance = null;
        }

        /// <summary>
        /// Closes the application. Stops play in editor.
        /// </summary>
        public static void CloseApplication()
        {
#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#endif
            Application.Quit();
        }

        /// <summary>
        /// Navigates to the appropriate scene if not already loaded
        /// </summary>
        public static void NavigateTo(NavigationMode navigationMode)
        {
            if (_instance == null)
            {
                throw new NullReferenceException("No instance of NavigationManager exists");
            }

            if (_instance._currentNavigationMode == navigationMode)
            {
                // No need to change modes, we are already in this mode
                return;
            }

            switch (navigationMode)
            {
                case NavigationMode.Main:
                    SceneManager.LoadScene(_instance._MainScene, LoadSceneMode.Single);
                    break;
                case NavigationMode.Game:
                    SceneManager.LoadScene(_instance._GameScene, LoadSceneMode.Single);
                    break;
            }

            _instance._currentNavigationMode = navigationMode;
        }
    }
}